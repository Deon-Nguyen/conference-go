import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    res = requests.get(f"https://api.pexels.com/v1/search?query={city}+{state}",
        headers={
            "Authorization": PEXELS_API_KEY
        }
    )
    data = json.loads(res.text)
    return data["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    res = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={city}+{state}&limit=1&appid={OPEN_WEATHER_API_KEY}",
        headers={
            "Authorization": OPEN_WEATHER_API_KEY
        }
    )
    data = json.loads(res.text)
    lon = data[0]["lon"]
    lat = data[0]["lat"]
    res1 = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}")
    location = json.loads(res1.text)
    return location["weather"][0]["description"], location["main"]["temp"]