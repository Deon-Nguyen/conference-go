from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from events.models import Conference, Status
import json
from django.views.decorators.http import require_http_methods


class StatusEncoder(ModelEncoder):
    model = Status
    properties = ["name"]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "status"
    ]
    encoders = {
        "status": StatusEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentation = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(name=content["conference"])
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invaild conference"},
                safe=False
            )

  
class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

@require_http_methods(["DELETE", "GET", "POST"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0})
    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )